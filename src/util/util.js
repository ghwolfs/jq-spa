import $ from './jquery.js';

const head = document.getElementsByTagName("head")[0];
const isOpera = typeof opera !== 'undefined' && opera.toString() === '[object Opera]';

/**
 * 辅助工具类
 */
let functions = {
	/**
	 * 加载一个script并返回Deferred延迟对象，加载完毕后会执行回调
	 * @param {Object} scriptNode script节点对象，不能为null
	 */
	loadScript(scriptNode) {
		var node = document.createElement('script');
		node.charset = "UTF-8";
		node.src = scriptNode.src;
		if (scriptNode.defer) {
			node.defer = true ;
		}
		if ($(scriptNode).attr('async') !== void 0) {
			node.async = scriptNode.async;
		} else {
			// 同步执行避免后续脚本过早执行
			node.async = false ;
		}
		
		head.appendChild(node);
		
		var def = $.Deferred();
		
		if (node.attachEvent && !(node.attachEvent.toString && node.attachEvent.toString().indexOf('[native code') < 0) && !isOpera) {
			node.attachEvent('onreadystatechange', function(e) {
				def.resolve(scriptNode.url,e);
			});
		} else {
			node.addEventListener('load', function(e) {
				def.resolve(scriptNode.url,e);
			}, false);
		}
		return def ;
	},
	
	/**
	 * 将一个dom追加到head中
	 * @param {Object} dom 要放置的dom
	 */
	appendToHead(dom) {
		head.appendChild(dom);
	},
	/**
	 * 冻结对象
	 * @param {Object} obj 要冻结的对象
	 */
	freeze: Object.freeze || function(obj){
		return obj ;
	}
}

/**
 * 获取一个空的对象
 * 如果支持Object.create(null)，则会使用这种方式创建，否则返回{}
 */
functions.emptyObject = Object.create ? function(){
	return Object.create(null);
} : function(){
	return {};
}


export default functions ;