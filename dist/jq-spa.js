(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("jquery"));
	else if(typeof define === 'function' && define.amd)
		define(["jquery"], factory);
	else if(typeof exports === 'object')
		exports["jq-spa"] = factory(require("jquery"));
	else
		root["jq-spa"] = factory(root["jquery"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE__3__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;// doT.js
// 2011-2014, Laura Doktorova, https://github.com/olado/doT
// Licensed under the MIT license.

(function () {
	"use strict";

	var doT = {
		name: "doT",
		version: "1.1.1",
		templateSettings: {
			evaluate:    /\{\{([\s\S]+?(\}?)+)\}\}/g,
			interpolate: /\{\{=([\s\S]+?)\}\}/g,
			encode:      /\{\{!([\s\S]+?)\}\}/g,
			use:         /\{\{#([\s\S]+?)\}\}/g,
			useParams:   /(^|[^\w$])def(?:\.|\[[\'\"])([\w$\.]+)(?:[\'\"]\])?\s*\:\s*([\w$\.]+|\"[^\"]+\"|\'[^\']+\'|\{[^\}]+\})/g,
			define:      /\{\{##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\}\}/g,
			defineParams:/^\s*([\w$]+):([\s\S]+)/,
			conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,
			iterate:     /\{\{~\s*(?:\}\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\}\})/g,
			varname:	"it",
			strip:		true,
			append:		true,
			selfcontained: false,
			doNotSkipEncoded: false
		},
		template: undefined, //fn, compile template
		compile:  undefined, //fn, for express
		log: true
	}, _globals;

	doT.encodeHTMLSource = function(doNotSkipEncoded) {
		var encodeHTMLRules = { "&": "&#38;", "<": "&#60;", ">": "&#62;", '"': "&#34;", "'": "&#39;", "/": "&#47;" },
			matchHTML = doNotSkipEncoded ? /[&<>"'\/]/g : /&(?!#?\w+;)|<|>|"|'|\//g;
		return function(code) {
			return code ? code.toString().replace(matchHTML, function(m) {return encodeHTMLRules[m] || m;}) : "";
		};
	};

	_globals = (function(){ return this || (0,eval)("this"); }());

	/* istanbul ignore else */
	if ( true && module.exports) {
		module.exports = doT;
	} else if (true) {
		!(__WEBPACK_AMD_DEFINE_RESULT__ = (function(){return doT;}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}

	var startend = {
		append: { start: "'+(",      end: ")+'",      startencode: "'+encodeHTML(" },
		split:  { start: "';out+=(", end: ");out+='", startencode: "';out+=encodeHTML(" }
	}, skip = /$^/;

	function resolveDefs(c, block, def) {
		return ((typeof block === "string") ? block : block.toString())
		.replace(c.define || skip, function(m, code, assign, value) {
			if (code.indexOf("def.") === 0) {
				code = code.substring(4);
			}
			if (!(code in def)) {
				if (assign === ":") {
					if (c.defineParams) value.replace(c.defineParams, function(m, param, v) {
						def[code] = {arg: param, text: v};
					});
					if (!(code in def)) def[code]= value;
				} else {
					new Function("def", "def['"+code+"']=" + value)(def);
				}
			}
			return "";
		})
		.replace(c.use || skip, function(m, code) {
			if (c.useParams) code = code.replace(c.useParams, function(m, s, d, param) {
				if (def[d] && def[d].arg && param) {
					var rw = (d+":"+param).replace(/'|\\/g, "_");
					def.__exp = def.__exp || {};
					def.__exp[rw] = def[d].text.replace(new RegExp("(^|[^\\w$])" + def[d].arg + "([^\\w$])", "g"), "$1" + param + "$2");
					return s + "def.__exp['"+rw+"']";
				}
			});
			var v = new Function("def", "return " + code)(def);
			return v ? resolveDefs(c, v, def) : v;
		});
	}

	function unescape(code) {
		return code.replace(/\\('|\\)/g, "$1").replace(/[\r\t\n]/g, " ");
	}

	doT.template = function(tmpl, c, def) {
		c = c || doT.templateSettings;
		var cse = c.append ? startend.append : startend.split, needhtmlencode, sid = 0, indv,
			str  = (c.use || c.define) ? resolveDefs(c, tmpl, def || {}) : tmpl;

		str = ("var out='" + (c.strip ? str.replace(/(^|\r|\n)\t* +| +\t*(\r|\n|$)/g," ")
					.replace(/\r|\n|\t|\/\*[\s\S]*?\*\//g,""): str)
			.replace(/'|\\/g, "\\$&")
			.replace(c.interpolate || skip, function(m, code) {
				return cse.start + unescape(code) + cse.end;
			})
			.replace(c.encode || skip, function(m, code) {
				needhtmlencode = true;
				return cse.startencode + unescape(code) + cse.end;
			})
			.replace(c.conditional || skip, function(m, elsecase, code) {
				return elsecase ?
					(code ? "';}else if(" + unescape(code) + "){out+='" : "';}else{out+='") :
					(code ? "';if(" + unescape(code) + "){out+='" : "';}out+='");
			})
			.replace(c.iterate || skip, function(m, iterate, vname, iname) {
				if (!iterate) return "';} } out+='";
				sid+=1; indv=iname || "i"+sid; iterate=unescape(iterate);
				return "';var arr"+sid+"="+iterate+";if(arr"+sid+"){var "+vname+","+indv+"=-1,l"+sid+"=arr"+sid+".length-1;while("+indv+"<l"+sid+"){"
					+vname+"=arr"+sid+"["+indv+"+=1];out+='";
			})
			.replace(c.evaluate || skip, function(m, code) {
				return "';" + unescape(code) + "out+='";
			})
			+ "';return out;")
			.replace(/\n/g, "\\n").replace(/\t/g, '\\t').replace(/\r/g, "\\r")
			.replace(/(\s|;|\}|^|\{)out\+='';/g, '$1').replace(/\+''/g, "");
			//.replace(/(\s|;|\}|^|\{)out\+=''\+/g,'$1out+=');

		if (needhtmlencode) {
			if (!c.selfcontained && _globals && !_globals._encodeHTML) _globals._encodeHTML = doT.encodeHTMLSource(c.doNotSkipEncoded);
			str = "var encodeHTML = typeof _encodeHTML !== 'undefined' ? _encodeHTML : ("
				+ doT.encodeHTMLSource.toString() + "(" + (c.doNotSkipEncoded || '') + "));"
				+ str;
		}
		try {
			return new Function(c.varname, str);
		} catch (e) {
			/* istanbul ignore else */
			if (typeof console !== "undefined") console.log("Could not create a template function: " + str);
			throw e;
		}
	};

	doT.compile = function(tmpl, def) {
		return doT.template(tmpl, null, def);
	};
}());


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;// doT.js
// 2011-2014, Laura Doktorova, https://github.com/olado/doT
// Licensed under the MIT license.

(function () {
	"use strict";

	var doT = {
		name: "doT",
		version: "1.1.1",
		templateSettings: {
			evaluate:    /\{\{([\s\S]+?(\}?)+)\}\}/g,
			interpolate: /\{\{=([\s\S]+?)\}\}/g,
			encode:      /\{\{!([\s\S]+?)\}\}/g,
			use:         /\{\{#([\s\S]+?)\}\}/g,
			useParams:   /(^|[^\w$])def(?:\.|\[[\'\"])([\w$\.]+)(?:[\'\"]\])?\s*\:\s*([\w$\.]+|\"[^\"]+\"|\'[^\']+\'|\{[^\}]+\})/g,
			define:      /\{\{##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\}\}/g,
			defineParams:/^\s*([\w$]+):([\s\S]+)/,
			conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,
			iterate:     /\{\{~\s*(?:\}\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\}\})/g,
			varname:	"it",
			strip:		true,
			append:		true,
			selfcontained: false,
			doNotSkipEncoded: false
		},
		template: undefined, //fn, compile template
		compile:  undefined, //fn, for express
		log: true
	}, _globals;

	doT.encodeHTMLSource = function(doNotSkipEncoded) {
		var encodeHTMLRules = { "&": "&#38;", "<": "&#60;", ">": "&#62;", '"': "&#34;", "'": "&#39;", "/": "&#47;" },
			matchHTML = doNotSkipEncoded ? /[&<>"'\/]/g : /&(?!#?\w+;)|<|>|"|'|\//g;
		return function(code) {
			return code ? code.toString().replace(matchHTML, function(m) {return encodeHTMLRules[m] || m;}) : "";
		};
	};

	_globals = (function(){ return this || (0,eval)("this"); }());

	/* istanbul ignore else */
	if ( true && module.exports) {
		module.exports = doT;
	} else if (true) {
		!(__WEBPACK_AMD_DEFINE_RESULT__ = (function(){return doT;}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}

	var startend = {
		append: { start: "'+(",      end: ")+'",      startencode: "'+encodeHTML(" },
		split:  { start: "';out+=(", end: ");out+='", startencode: "';out+=encodeHTML(" }
	}, skip = /$^/;

	function resolveDefs(c, block, def) {
		return ((typeof block === "string") ? block : block.toString())
		.replace(c.define || skip, function(m, code, assign, value) {
			if (code.indexOf("def.") === 0) {
				code = code.substring(4);
			}
			if (!(code in def)) {
				if (assign === ":") {
					if (c.defineParams) value.replace(c.defineParams, function(m, param, v) {
						def[code] = {arg: param, text: v};
					});
					if (!(code in def)) def[code]= value;
				} else {
					new Function("def", "def['"+code+"']=" + value)(def);
				}
			}
			return "";
		})
		.replace(c.use || skip, function(m, code) {
			if (c.useParams) code = code.replace(c.useParams, function(m, s, d, param) {
				if (def[d] && def[d].arg && param) {
					var rw = (d+":"+param).replace(/'|\\/g, "_");
					def.__exp = def.__exp || {};
					def.__exp[rw] = def[d].text.replace(new RegExp("(^|[^\\w$])" + def[d].arg + "([^\\w$])", "g"), "$1" + param + "$2");
					return s + "def.__exp['"+rw+"']";
				}
			});
			var v = new Function("def", "return " + code)(def);
			return v ? resolveDefs(c, v, def) : v;
		});
	}

	function unescape(code) {
		return code.replace(/\\('|\\)/g, "$1").replace(/[\r\t\n]/g, " ");
	}

	doT.template = function(tmpl, c, def) {
		c = c || doT.templateSettings;
		var cse = c.append ? startend.append : startend.split, needhtmlencode, sid = 0, indv,
			str  = (c.use || c.define) ? resolveDefs(c, tmpl, def || {}) : tmpl;

		str = ("var out='" + (c.strip ? str.replace(/(^|\r|\n)\t* +| +\t*(\r|\n|$)/g," ")
					.replace(/\r|\n|\t|\/\*[\s\S]*?\*\//g,""): str)
			.replace(/'|\\/g, "\\$&")
			.replace(c.interpolate || skip, function(m, code) {
				return cse.start + unescape(code) + cse.end;
			})
			.replace(c.encode || skip, function(m, code) {
				needhtmlencode = true;
				return cse.startencode + unescape(code) + cse.end;
			})
			.replace(c.conditional || skip, function(m, elsecase, code) {
				return elsecase ?
					(code ? "';}else if(" + unescape(code) + "){out+='" : "';}else{out+='") :
					(code ? "';if(" + unescape(code) + "){out+='" : "';}out+='");
			})
			.replace(c.iterate || skip, function(m, iterate, vname, iname) {
				if (!iterate) return "';} } out+='";
				sid+=1; indv=iname || "i"+sid; iterate=unescape(iterate);
				return "';var arr"+sid+"="+iterate+";if(arr"+sid+"){var "+vname+","+indv+"=-1,l"+sid+"=arr"+sid+".length-1;while("+indv+"<l"+sid+"){"
					+vname+"=arr"+sid+"["+indv+"+=1];out+='";
			})
			.replace(c.evaluate || skip, function(m, code) {
				return "';" + unescape(code) + "out+='";
			})
			+ "';return out;")
			.replace(/\n/g, "\\n").replace(/\t/g, '\\t').replace(/\r/g, "\\r")
			.replace(/(\s|;|\}|^|\{)out\+='';/g, '$1').replace(/\+''/g, "");
			//.replace(/(\s|;|\}|^|\{)out\+=''\+/g,'$1out+=');

		if (needhtmlencode) {
			if (!c.selfcontained && _globals && !_globals._encodeHTML) _globals._encodeHTML = doT.encodeHTMLSource(c.doNotSkipEncoded);
			str = "var encodeHTML = typeof _encodeHTML !== 'undefined' ? _encodeHTML : ("
				+ doT.encodeHTMLSource.toString() + "(" + (c.doNotSkipEncoded || '') + "));"
				+ str;
		}
		try {
			return new Function(c.varname, str);
		} catch (e) {
			/* istanbul ignore else */
			if (typeof console !== "undefined") console.log("Could not create a template function: " + str);
			throw e;
		}
	};

	doT.compile = function(tmpl, def) {
		return doT.template(tmpl, null, def);
	};
}());


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(5);


/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__3__;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {/* globals __webpack_amd_options__ */
module.exports = __webpack_amd_options__;

/* WEBPACK VAR INJECTION */}.call(this, {}))

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./src/util/jquery.js
var jquery_$ = __webpack_require__(3) || jQuery;
/* harmony default export */ var jquery = (jquery_$);
// EXTERNAL MODULE: ./node_modules/dot/doT.js
var doT = __webpack_require__(0);
var doT_default = /*#__PURE__*/__webpack_require__.n(doT);

// EXTERNAL MODULE: ./node_modules/doT/doT.js
var doT_doT = __webpack_require__(1);
var doT_doT_default = /*#__PURE__*/__webpack_require__.n(doT_doT);

// CONCATENATED MODULE: ./src/config/config.js

var configuration = {
  // 是否不做缓存，开发阶段设置为true方便调试
  noCache: false,
  // 设置别名，会在某些地方用到
  alias: {},
  // 是否忽略异常
  ignoreError: false,
  // 用于hash route的配置，当hash变化时，触发的事件，你需要做的是返回一个jqueryDom对象，用于渲染页面
  // 如果不返回jquery Dom对象则不做渲染处理
  getHashChangeRenderDom: function getHashChangeRenderDom() {},
  // 模板规则配置，需要配置模板初始化(init)、预渲染(beforeMount)、渲染(mounted)三部分操作
  // 默认使用的是doT模板引擎，此配置是用于扩展更多自定义的组件/模板解析逻辑，例如你可以集成vue。
  // 在template中除了init方法外，this对象都是组件生命周期方法中会得到的this对象。你可以对其进行扩展或值的读取
  template: {
    /**
     * 初始化模板html，这是在第一次获取到模板html的时候进行的处理操作。
     * 你可以预处理一些html，例如doT可以对模板进行编译缓存。或也可以不进行任何处理
     * @param {Object} templateHtml 模板html，一定不是null
     * @return 返回初始化之后的对象，如果无需进行初始化，可不返回或返回templateHtml，返回结果会传递给preRender
     */
    init: function init(templateHtml) {
      // 这是doT模板引擎的处理操作
      return doT_doT_default.a.template(templateHtml);
    },

    /**
     * 模板解析对象构造，而后将这个对象返回。
     * 例如doT可以是执行解析后html，vue可以使new Vue。
     * 总之这一步你需要对模板进行处理，但不在页面渲染。
     * 返回结果将会交给render方法。
     * 
     * @param {Object} initReturnObj init方法中的返回参数，如果init方法没有返回任何值，则是templateHTML
     * @param {Object} data 模板中的data部分的数据
     * @return 返回一个模板解析后的对象，render和beforeMount方法中将会拿到这个对象
     */
    preRender: function preRender(initReturnObj, data) {
      // 这是doT模板引擎的处理操作
      return initReturnObj(data);
    },

    /**
     * 渲染处理，将dom最终展示到页面上的操作
     * 
     * @param {Object} preRenderObject preRender方法的返回对象
     * @param {Object} dom HtmlElement对象
     * @param {Object} jqDom jqueryDom对象
     */
    render: function render(preRenderObject, dom, jqDom) {
      // 这是doT模板引擎的处理操作
      var $doms = $('<div/>').append($.parseHTML(preRenderObject, false)).children();
      jqDom.empty().append($doms);
    },

    /**
     * 所有处理完毕，在准备执行mounted拦截器之前，要做的事情。
     * @param {Object} preRenderObject preRender方法的返回对象
     */
    beforeMount: function beforeMount(preRenderObject) {}
  }
};
var globalConfig = configuration;
/**
 * 覆盖默认配置，传递一个object覆盖默认配置
 * 或返回配置信息，不传参数则返回当前配置
 * @param {Object} cfg 配置对象
 * @return 如果不传参数，则返回当前配置信息
 */

function config(cfg) {
  if (arguments.length === 0) return configuration;
  if (!cfg) return;

  for (var x in configuration) {
    if (cfg[x] != null) {
      configuration[x] = cfg[x];
    }
  }
}
/**
 * 替换一个字符串的前缀别名
 * @param {Object} str 要替换的字符串
 * @return 返回替换结束后的字符串
 */

function replacePrefix(str) {
  if (!str) return str;

  if ('string' !== str) {
    str += '';
  }

  var maxPrefixLength = 0,
      aliasValue;

  for (var x in configuration.alias) {
    var i = str.indexOf(x);

    if (i === 0 && x.length > maxPrefixLength) {
      maxPrefixLength = x.length;
      aliasValue = configuration.alias[x];
    }
  }

  return maxPrefixLength ? aliasValue + str.substr(maxPrefixLength) : str;
}
/**
 * 输出一个错误，如果ignoreError设置为true，则不作任何事情
 * @param {Object} message 要输出的信息
 */

function error(message) {
  if (configuration.ignoreError) {
    return;
  } else {
    throw message;
  }
}
/**
 * 是否禁用缓存
 */

function isNoCache() {
  return configuration.noCache;
}
// CONCATENATED MODULE: ./src/util/util.js

var head = document.getElementsByTagName("head")[0];
var isOpera = typeof opera !== 'undefined' && opera.toString() === '[object Opera]';
/**
 * 辅助工具类
 */

var functions = {
  /**
   * 加载一个script并返回Deferred延迟对象，加载完毕后会执行回调
   * @param {Object} scriptNode script节点对象，不能为null
   */
  loadScript: function loadScript(scriptNode) {
    var node = document.createElement('script');
    node.charset = "UTF-8";
    node.src = scriptNode.src;

    if (scriptNode.defer) {
      node.defer = true;
    }

    if (jquery(scriptNode).attr('async') !== void 0) {
      node.async = scriptNode.async;
    } else {
      // 同步执行避免后续脚本过早执行
      node.async = false;
    }

    head.appendChild(node);
    var def = jquery.Deferred();

    if (node.attachEvent && !(node.attachEvent.toString && node.attachEvent.toString().indexOf('[native code') < 0) && !isOpera) {
      node.attachEvent('onreadystatechange', function (e) {
        def.resolve(scriptNode.url, e);
      });
    } else {
      node.addEventListener('load', function (e) {
        def.resolve(scriptNode.url, e);
      }, false);
    }

    return def;
  },

  /**
   * 将一个dom追加到head中
   * @param {Object} dom 要放置的dom
   */
  appendToHead: function appendToHead(dom) {
    head.appendChild(dom);
  },

  /**
   * 冻结对象
   * @param {Object} obj 要冻结的对象
   */
  freeze: Object.freeze || function (obj) {
    return obj;
  }
};
/**
 * 获取一个空的对象
 * 如果支持Object.create(null)，则会使用这种方式创建，否则返回{}
 */

functions.emptyObject = Object.create ? function () {
  return Object.create(null);
} : function () {
  return {};
};
/* harmony default export */ var util = (functions);
// CONCATENATED MODULE: ./src/cmp/Component.js
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



 // 缓存已经加载的外部资源路径，避免不同组件同一个资源重复加载

var LOADED_PATH = []; // 禁止出现的自定义方法名称

var PROHIBITED_METHOD_NAME = ['$render', '$methods', '$options']; // 一个表示成功的jquery延迟对象

var RESOLVE_DEF = jquery.Deferred();
RESOLVE_DEF.resolve();
/**
 * 获取一个原型链是指定methods对象的一个类
 */

function createRenderObject(methods) {
  function RenderObject() {}

  var propMethods = jquery.extend({}, methods);

  if (methods) {
    for (var x = 0; x < PROHIBITED_METHOD_NAME.length; x++) {
      var k = PROHIBITED_METHOD_NAME[x];

      if (propMethods[k]) {
        error("不允许出现[" + k + "]组件方法名，这是一个内置的方法/属性名！");
        delete propMethods[k];
      }
    }

    jquery.extend(RenderObject.prototype, propMethods);
  }

  RenderObject.prototype.$methods = propMethods;
  return RenderObject;
}
/**
 * 组件类、组件预渲染，预处理结果封装类
 */


var Component_Component = /*#__PURE__*/function () {
  /**
   * @param {Object} html 组件html源代码
   */
  function Component(html) {
    var _this = this;

    _classCallCheck(this, Component);

    // 主脚本
    this.localJs = void 0; // 组件模板

    this.template = void 0; // 模板引擎对象

    this.tmpObject; // 组件配置，通过localJs脚本返回的object获取

    this.config = util.emptyObject(); // 是否已经执行了初始化

    this.inited = false; // 组件脚本中的this创建类

    this.RenderObject = void 0;
    var roots = jquery('<div/>').append(jquery.parseHTML(html, true)).children(); // 避免读取到文本、注释节点

    roots.each(function (i, root) {
      var $root = jquery(root);

      if (root.nodeName == 'SCRIPT') {
        if (_this.localJs) {
          error("错误的组件格式，jQuery-SPA要求组件中只能有一个内部执行的script节点。但是却有多个：\n" + html);
          return;
        }

        _this.localJs = root.text || '';
      } else if (root.nodeName == 'STYLE') {
        util.appendToHead(root);
      } else {
        if (_this.template) {
          error("错误的组件格式，jQuery-SPA要求组件中只能有一个模板节点。但是却有多个：\n" + html);
          return;
        }

        _this.template = jquery.trim(root.innerHTML);
      }
    });

    if (!this.template) {
      error("错误的组件格式，jQuery-SPA要求组件中必须有一个模板节点。但是却未找到或组件内元素为空：\n" + html);
    }

    this.tmpObject = globalConfig.template.init.call(void 0, this.template || '') || this.template;
  }
  /**
   * 将当前组件渲染到指定dom中(类似jquery.load方法)
   * @param {Object} $dom 要渲染的jqueryDom
   * @param {Object} params 父组件传递给当前组件的参数，object类型
   * @param {Object} queryParam url参数,object类型
   */


  _createClass(Component, [{
    key: "render",
    value: function render($dom, params, queryParam) {
      var _this2 = this;

      if (this.inited) {
        if (this.inited === true) {
          this._render($dom, params, queryParam);

          return RESOLVE_DEF;
        } else {
          var def = jquery.Deferred();
          this.inited.push([def, arguments]);
          return def;
        }
      } else {
        this.inited = [];
        return this._init().done(function () {
          var rs = _this2.inited;
          _this2.inited = true;

          _this2._render($dom, params, queryParam);

          if (rs && rs.length) {
            var _loop = function _loop(x) {
              _this2._render.apply(_this2, rs[x][1]);

              setTimeout(function () {
                rs[x][0].resolve();
              }, 0);
            };

            for (var x = 0; x < rs.length; x++) {
              _loop(x);
            }
          }
        });
      }
    }
    /**
     * 渲染操作，仅内部调用
     */

  }, {
    key: "_render",
    value: function _render($dom, params, queryParam) {
      if (!($dom && $dom.jquery && $dom.length)) {
        return;
      }

      $dom = $dom.first();
      var data = typeof this.config.data == 'function' ? this.config.data() : util.emptyObject();
      var props = this.config.props;

      if (props && props.length && (params || queryParam)) {
        for (var x = 0; x < props.length; x++) {
          var k = props[x];
          var v = queryParam && queryParam[k] || params && params[k];

          if (v) {
            data[k] = v;
          }
        }
      }

      var that = new this.RenderObject();
      that.data = data;
      that.$query = queryParam || util.emptyObject();
      that.$options = this.config;
      that.$el = $dom[0];
      that.$jqDom = $dom;

      this._interceptor(that, 'created');

      var preRenderObject = globalConfig.template.preRender.call(that, this.tmpObject, data);

      this._interceptor(that, 'beforeMount');

      globalConfig.template.render.call(that, preRenderObject, $dom[0], $dom);
      var $refs = util.emptyObject();
      $dom.find('[ref]').each(function (i, d) {
        $refs[d.getAttribute('ref')] = d;
      });
      that.$refs = $refs;
      globalConfig.template.beforeMount.call(that, preRenderObject);

      this._interceptor(that, 'mounted');
    }
    /**
     * 执行拦截器，仅内部调用
     */

  }, {
    key: "_interceptor",
    value: function _interceptor(that, funName) {
      if (typeof this.config[funName] == 'function') {
        this.config[funName].call(that);
      }
    }
    /**
     * 初始化，仅允许内部调用
     */

  }, {
    key: "_init",
    value: function _init() {
      // 初始化组件配置
      if (this.localJs) {
        this.config = new Function("return " + jquery.trim(this.localJs))() || util.emptyObject();
        util.freeze(this.config);
        var initFunction = this.config.methods && this.config.methods.init;

        if (typeof initFunction === 'function') {
          initFunction.call(void 0);
        }

        this.RenderObject = createRenderObject(this.config.methods);
      }

      var def = jquery.Deferred();

      if (this.config.require && this.config.require.length) {
        window.require(this.config.require, function () {
          def.resolve();
        });
      } else {
        def.resolve();
      }

      return def;
    }
  }]);

  return Component;
}();

/* harmony default export */ var cmp_Component = (Component_Component);
// CONCATENATED MODULE: ./src/jq-spa.js





var version = '1.0.0';

(function ($, window) {
  window.doT = doT_default.a;
  var hasAmd = typeof define === 'function' && __webpack_require__(4);
  var cmpCache = util.emptyObject();
  var routeInited = false;
  var hashRouteNoHandlerFlag = false;
  /**
   * 初始化一个组件对象，并返回jQuery延迟对象
   * @param {String} url 处理过的html请求地址
   */

  function initComponent(url) {
    var def = $.Deferred();
    var cmp; // TODO config中增加配置，可以设置ajax属性，这里使用ajax方法
    // TODO 404等问题处理

    $.get(url, {}, $.noop, 'html').done(function (html) {
      cmp = new cmp_Component(html);
      cmpCache[url] = cmp;
      def.resolve(cmp);
    }).fail(function (jqXhr, status, msg) {
      def.reject(jqXhr, status, msg);
    });
    return def;
  }
  /**
   * 解析url，并取得一个对象，可以获取到url参数和url值
   * url参数一定不是空，如果没有则返回{}
   */


  function parseUrl(url) {
    var i = url.indexOf('?');
    var obj = {
      queryParam: util.emptyObject(),
      url: i === -1 ? url : url.substring(0, i)
    };

    if (i !== -1) {
      var query = url.substr(i + 1);
      var kvs = query.split('&');

      for (var x = 0; x < kvs.length; x++) {
        var point = kvs[x].indexOf('=');

        if (point !== -1) {
          var k = kvs[x].substring(0, point);
          var v = kvs[x].substring(point + 1);

          if (k) {
            obj.queryParam[k] = v;
          }
        }
      }
    }

    return obj;
  } // 初始化SPA全局对象


  $.spa = {
    version: version,
    config: config,
    render: function render($dom, url, params) {
      url = replacePrefix(url);

      if (!url) {
        return $.Deferred().reject('未指定url！');
      }

      var urlInfo = parseUrl(url);

      if (!isNoCache() && cmpCache[urlInfo.url]) {
        return cmpCache[urlInfo.url].render($dom, params, urlInfo.queryParam);
      } else {
        var def = $.Deferred();
        initComponent(urlInfo.url).done(function (cmp) {
          cmp.render($dom, params, urlInfo.queryParam).then(function () {
            def.resolve();
          }, function () {
            def.reject();
          });
        });
        return def;
      }
    },
    // hash 路由处理
    route: {
      changeHashOnly: function changeHashOnly(hash) {
        hashRouteNoHandlerFlag = true;
        location.hash = hash;
      },
      // 初始化hash路由功能
      init: function init() {
        if (routeInited) {
          return;
        }

        routeInited = true;

        function doHashChange() {
          if (hashRouteNoHandlerFlag) {
            hashRouteNoHandlerFlag = false;
            return;
          }

          var hashUrl;

          if (location.hash) {
            hashUrl = location.hash.substr(1);
          } else {
            return;
          }

          var $dom = globalConfig.getHashChangeRenderDom(hashUrl);

          if ($dom && $dom.jquery && $dom.render) {
            $dom.render(hashUrl);
          }
        }

        window.addEventListener('hashchange', doHashChange); // 第一次读取hash

        if (location.hash) {
          doHashChange();
        }
      }
    }
  };

  $.fn.render = function (url, params) {
    $.spa.render(this, url, params);
  };
})(jquery, window);

/* harmony default export */ var jq_spa = __webpack_exports__["default"] = (jquery.spa);

/***/ })
/******/ ]);
});