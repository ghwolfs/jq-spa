// 不需要npm run build,如果你安装了全局webpack-cli，直接运行webpack --mode production

const path = require('path');
const UnminifiedWebpackPlugin = require('unminified-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
// 安装babel
// npm install -D @babel/core babel-core @babel/preset-env


module.exports = {
	entry : ['./src/jq-spa.js'],
	output: {
		path: path.join(__dirname,'./dist'),
		filename: 'jq-spa.min.js',
		libraryTarget: 'umd',
		// amd模块名称为jq-spa
		library: 'jq-spa'
	},
	// 依赖但是不打包的库
	// 如果配合libraryTarget: 'umd'，那么就会出现require(['jquery',xxx])这样的代码
	externals: {
		jquery: 'jquery'
	},
	module:{
		rules:[
			{
				test:/\.js$/,
				use:{
					loader:'babel-loader',
					options: {
						presets:['@babel/preset-env']
					}
				},
				exclude:/node_modules/
			}
		]
	},
	plugins: [
		// clean dist
		new CleanWebpackPlugin(),
		// 同时打包压缩包和未压缩版
		new UnminifiedWebpackPlugin(),
	],
};